package com.epam.services;

import com.epam.dao.implementation.ServiceDAOImpl;
import com.epam.dao.implementation.UserDAOImpl;
import com.epam.dao.implementation.UserServiceListDAOImpl;
import com.epam.dao.interfaces.ServiceDAO;
import com.epam.dao.interfaces.UserDAO;
import com.epam.dao.interfaces.UserServiceListDAO;
import com.epam.entities.Service;
import com.epam.entities.User;

import java.sql.Connection;
import java.sql.SQLException;

public class AddNewServiceToUserService {
    private Connection connection;
    private String userEmail;
    private String serviceName;
    private UserDAO userService;
    private ServiceDAO serviceDAO;
    private UserServiceListDAO userserviceListDAO;

    public AddNewServiceToUserService() {
    }

    public AddNewServiceToUserService(String userEmail, String serviceName, Connection connection) throws SQLException {
        this.connection = connection;
        this.userEmail = userEmail;
        this.serviceName = serviceName;
        this.userService = new UserDAOImpl(connection);
        this.serviceDAO = new ServiceDAOImpl(connection);
        this.userserviceListDAO = new UserServiceListDAOImpl(connection);
    }

    public void addService() {
        User actUser = userService.get(userEmail);
        Service actService = serviceDAO.get(serviceName);
        if (!(actUser.getAmount() - actService.getTariff() < 0)) {
            userserviceListDAO.addServiceToUser(userEmail, serviceName);
            actUser.setAmount(actUser.getAmount() - actService.getTariff());
            actUser.setBlocked(false);
            userService.update(userEmail, actUser);
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
