<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/style.css">
    <title>Registration</title>
</head>
<body>

<div class="data">
    <a href="${pageContext.request.contextPath}/userRegistration?locale=en_ENG">ENG</a>
    <a href="${pageContext.request.contextPath}/userRegistration?locale=ru_RU">RUS</a>
</div>

<%@include file="top.jsp" %>

<h3>${regpage}</h3>

<p style="color: red;">${errorString}</p>

<%
    Cookie[] cookies = request.getCookies();
    String sessionIDInCookie = "";
    for (Cookie c : cookies) {
        if (c.getName().equals("session_id")) {
            sessionIDInCookie = c.getValue();
        }
    }

    if (sessionIDInCookie.equals(session.getId())) {
        response.sendRedirect("user_home.jsp");
    }
%>


<form method="POST" action="${pageContext.request.contextPath}/userRegistration">
    <table border="0">
        <tr>
            <td>${fNameField}</td>
            <td><input type="text" name="f_name" value="${user.firstName}"/></td>
        </tr>
        <tr>
            <td>${sNameField}</td>
            <td><input type="text" name="s_name" value="${user.secondName}"/></td>
        </tr>
        <tr>
            <td>${emailField}</td>
            <td><input type="text" name="email" value="${user.email}"/></td>
        </tr>
        <tr>
            <td>${passField}</td>
            <td><input type="password" name="password" value="${user.password}"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value=${submitbutton}>
                <a href="${pageContext.request.contextPath}/">${cancelbutton}</a>
            </td>
        </tr>
    </table>
</form>
<div>
    <jsp:include page="copyrightTag.jsp"></jsp:include>
</div>
</body>
</html>
