ALTER TABLE users_services
ADD CONSTRAINT users_services_fk
FOREIGN KEY (service) REFERENCES services (name)
  ON DELETE CASCADE
  ON UPDATE CASCADE;