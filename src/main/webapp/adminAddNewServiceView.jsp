<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Add New Service Page</title>
</head>
<body>
<%
    Cookie[] cookies = request.getCookies();
    String roleNameCookie = "";
    for (Cookie c : cookies) {
        if (c.getName().equals("role")) {
            roleNameCookie = c.getValue();
        }
    }

    if (!(roleNameCookie.equals("ADMINISTRATOR"))) {
        response.sendRedirect("userInfoPage.jsp");
    }
%>
<jsp:include page="adminMenu.jsp"></jsp:include>
<br>
<h3>Add New Service</h3>
<form method="POST" action="${pageContext.request.contextPath}/addNewService">
    <table border="1">
        <tr>
            <td><p>Service Name</p></td>
            <td><input type="text" name="newServiceName" value="${newServiceName}"/></td>
        </tr>
        <tr>
            <td><p>Service Tariff</p></td>
            <td><input type="text" name="newServiceTariff" value="${newServiceTariff}"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="Submit"/>
                <a href="${pageContext.request.contextPath}/">Cancel</a>
            </td>
        </tr>
    </table>
</form>
<jsp:include page="copyrightTag.jsp"></jsp:include>
</body>
</html>
