package com.epam.servlets;

import com.epam.connection.Pool;
import com.epam.services.AddNewServiceToUserService;
import com.epam.services.payment.AmountControlService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;

@WebServlet(urlPatterns = "/addServiceToUser")
public class AddServiceUserServlet extends HttpServlet {
    private static Logger LOGGER = LoggerFactory.getLogger(AddServiceUserServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String serviceName = req.getParameter("serviceName");
        String email = req.getParameter("email");
        try {
            Connection connection = Pool.getConnection();
            AddNewServiceToUserService addService = new AddNewServiceToUserService(email, serviceName, connection);
            addService.addService();

            AmountControlService amountControlService;
            amountControlService = new AmountControlService(email, connection);
            amountControlService.run();
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }

        LOGGER.info("User " + email + "   added " + serviceName);
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/userInfoPage.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
