package com.epam.servlets;

import com.epam.connection.Pool;
import com.epam.services.download.DownloadHtmlFileService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/downloadServiceList")
public class DownloadServiceTariffServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String url = req.getParameter("url");
        DownloadHtmlFileService downloadHtmlFileService = new DownloadHtmlFileService(Pool.getConnection());
        downloadHtmlFileService.downloadFile(url);
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/userInfoPage.jsp");
        dispatcher.forward(req, resp);
    }
}
