package com.epam.services.payment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Timer;

public class TimerRunner {
    private Connection connection;
    private long runPeriod = 60000;
    private Logger LOGGER = LoggerFactory.getLogger(TimerRunner.class);
    private Timer timer = new Timer();
    private PeriodTermServiceCheckingService service = new PeriodTermServiceCheckingService(connection);

    public TimerRunner(Connection connection) throws SQLException {
        this.connection = connection;
    }

    public TimerRunner(long runPeriod) throws SQLException {
        this.runPeriod = runPeriod;
    }

    public void run() {
        timer.schedule(service, 0, runPeriod);
        LOGGER.info(TimerRunner.class + " started. Run every " + runPeriod + " secconds.");
    }
}
