package com.epam.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Calendar;


public class CopyrightTag extends TagSupport {

    private String name;

    @Override
    public int doStartTag() throws JspException {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        String copyright = "© " + year + " ";
        try {
            pageContext.getOut().write(copyright + name);
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

    @Override
    public void release() {
        super.release();
        this.name = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
