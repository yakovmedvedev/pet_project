package com.epam.services.download;

import java.io.IOException;

public interface FileDownload {
    void downloadFile() throws IOException;

    void downloadFile(String url) throws IOException;
}
