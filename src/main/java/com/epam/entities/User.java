package com.epam.entities;

import java.text.SimpleDateFormat;
import java.util.Date;

public class User {

    public static final String CLIENT = "CLIENT";
    public static final String ADMIN = "ADMINISTRATOR";

    private long userID;
    private String firstName;
    private String secondName;
    private String email;
    private String password;
    private int amount;
    private boolean isConfirmed;
    private boolean isBlocked;
    private String role;
    private String registrationDate;

    public User() {
        initDate();
    }

    public User(String firstName, String secondName, String email, String password) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.email = email;
        this.password = password;
        initDate();
    }

    public User(String firstName, String secondName, String email, String password, String role) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.email = email;
        this.password = password;
        this.role = role;
        initDate();
    }

    public User(long userID,
                String firstName,
                String secondName,
                String email,
                String password,
                int amount,
                boolean isConfirmed,
                boolean isBlocked,
                String role,
                String registrationDate) {
        this.userID = userID;
        this.firstName = firstName;
        this.secondName = secondName;
        this.email = email;
        this.password = password;
        this.amount = amount;
        this.isConfirmed = isConfirmed;
        this.isBlocked = isBlocked;
        this.role = role;
        this.registrationDate = registrationDate;
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(boolean confirmed) {
        isConfirmed = confirmed;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public void setRegistredDate(Date registredDate) {
        String pattern = "dd.MM.yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(registredDate);
        this.registrationDate = date;
    }

    public void initDate() {
        String pattern = "dd.MM.yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        this.registrationDate = date;
    }

    @Override
    public String toString() {
        return "User{" +
                "userID=" + userID +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", amount=" + amount +
                ", isConfirmed=" + isConfirmed +
                ", isBlocked=" + isBlocked +
                ", role='" + role + '\'' +
                ", registrationDate='" + registrationDate + '\'' +
                '}';
    }
}
