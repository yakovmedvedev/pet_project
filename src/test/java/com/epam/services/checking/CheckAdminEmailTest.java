package com.epam.services.checking;

import com.epam.connection.PoolH2DB;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.assertTrue;

public class CheckAdminEmailTest {
    private Connection connection = PoolH2DB.getConnection();

    public CheckAdminEmailTest() {
    }

    @Before
    public void clearTableBefore() throws SQLException {
        try (Statement ps = connection.createStatement()) {
            ps.execute("TRUNCATE TABLE USERS");
        }
    }

    @Test
    public void checkIsThisAdminEmail() throws Exception {
        PreparedStatement statement = connection.prepareStatement
                ("INSERT INTO users (f_name,s_name,email,password,role,registration_date) VALUES (?,?,?,?,?,?)");
        statement.setString(1, "Bob");
        statement.setString(2, "Polson");
        statement.setString(3, "admin@gmail.com");
        statement.setString(4, "333333");
        statement.setString(5, "ADMINISTRATOR");
        statement.setString(6, "01.02.2018");
        statement.execute();
        statement.close();
        new CheckAdminEmail(connection);
        assertTrue(CheckAdminEmail.check("admin@gmail.com"));
    }

    @After
    public void clearTable() throws SQLException {
        try (Statement ps = connection.createStatement()) {
            ps.execute("TRUNCATE TABLE USERS");
        }
    }
}