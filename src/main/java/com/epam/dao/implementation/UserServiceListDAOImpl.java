package com.epam.dao.implementation;

import com.epam.dao.interfaces.UserServiceListDAO;
import com.epam.entities.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class UserServiceListDAOImpl implements UserServiceListDAO {

    private Connection connection;

    public UserServiceListDAOImpl() {
    }

    public UserServiceListDAOImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void addServiceToUser(String userEmail, String serviceName) {
        if (!checkIfServiceAlreadyExist(userEmail, serviceName)) {
            try {
                PreparedStatement statement = connection.prepareStatement(
                        "INSERT INTO users_services (email,service,start_term,end_term) VALUES (?,?,?,?)");
                statement.setString(1, userEmail);
                statement.setString(2, serviceName);

                String pattern = "dd.MM.yyyy HH:mm:ss";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                Date currentDate = new Date();
                String startDate = simpleDateFormat.format(currentDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(currentDate);
                calendar.add(Calendar.MINUTE, 1);
                Date endDate = calendar.getTime();
                String endDateString = simpleDateFormat.format(endDate);

                statement.setString(3, startDate);
                statement.setString(4, endDateString);
                statement.execute();
                statement.close();
            } catch (SQLException e) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                e.printStackTrace();
            } finally {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public Service getServiceToUser(String userEmail, String serviceName) {
        ServiceDAOImpl serviceDAO = new ServiceDAOImpl(connection);
        Service service = null;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT service FROM users_services WHERE email=? AND service=?");
            statement.setString(1, userEmail);
            statement.setString(2, serviceName);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                service = serviceDAO.get(resultSet.getString("service"));
            }
            statement.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return service;
    }

    @Override
    public void removeServiceFromUser(String userEmail, String serviceName) {
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "DELETE FROM users_services WHERE email=? AND service=?");
            statement.setString(1, userEmail);
            statement.setString(2, serviceName);
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public List<Service> getAllServiceToUser(String userEmail) {
        ServiceDAOImpl serviceDAO = new ServiceDAOImpl(connection);
        List<Service> result = null;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT DISTINCT service FROM users_services WHERE email=?");
            statement.setString(1, userEmail);
            ResultSet resultSet = statement.executeQuery();
            result = new ArrayList<>();
            while (resultSet.next()) {
                if (resultSet.getString("service") != null) {
                    Service service = serviceDAO.get(resultSet.getString("service"));
                    result.add(service);
                }
            }
            statement.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    public List<Service> getAllServiceOrderedByName() {
        ServiceDAOImpl serviceDAO = new ServiceDAOImpl(connection);
        List<Service> result = null;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM services ORDER BY name");
            ResultSet resultSet = statement.executeQuery();
            result = new ArrayList<>();
            while (resultSet.next()) {
                Service service = serviceDAO.get(resultSet.getString("name"));
                result.add(service);
            }
            statement.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    public List<Service> getAllServiceOrderedByPrice() {
        ServiceDAOImpl serviceDAO = new ServiceDAOImpl(connection);
        List<Service> result = null;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM services ORDER BY tariff");
            ResultSet resultSet = statement.executeQuery();
            result = new ArrayList<>();
            while (resultSet.next()) {
                Service service = serviceDAO.get(resultSet.getString("name"));
                result.add(service);
            }
            statement.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private boolean checkIfServiceAlreadyExist(String userEmail, String serviceName) {
        boolean result = false;
        try {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT COUNT(*) FROM users_services WHERE email=? AND service=?");
            ps.setString(1, userEmail);
            ps.setString(2, serviceName);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                if (resultSet.getInt(1) >= 1) {
                    result = true;
                }
            }
            ps.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
