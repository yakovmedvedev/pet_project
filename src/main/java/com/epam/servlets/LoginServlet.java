package com.epam.servlets;

import com.epam.connection.Pool;
import com.epam.dao.implementation.UserDAOImpl;
import com.epam.entities.User;
import com.epam.services.checking.CheckAdminEmail;
import com.epam.services.checking.CheckUserConfirmed;
import com.epam.services.checking.CheckUserInDB;
import com.epam.services.payment.TimerRunner;
import com.epam.services.validation.UserLoginFieldsValidator;
import com.epam.util.LocalizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/userLogin")
public class LoginServlet extends HttpServlet {

    private static Logger LOGGER = LoggerFactory.getLogger(LoginServlet.class);
    private UserDAOImpl userService;

    public LoginServlet() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        RequestDispatcher dispatcher;
        LocalizationService localizationService;
        String localizationParam = request.getParameter("locale");
        if (localizationParam == null) {
            localizationService = new LocalizationService(request.getLocale());
        } else {
            localizationService = new LocalizationService(localizationParam);
        }

        session.setAttribute("emailField", localizationService.getLocaleString("emailField"));
        session.setAttribute("passField", localizationService.getLocaleString("passField"));
        session.setAttribute("submitbutton", localizationService.getLocaleString("submitbutton"));
        session.setAttribute("cancelbutton", localizationService.getLocaleString("cancelbutton"));
        session.setAttribute("regpage", localizationService.getLocaleString("regpage"));
        session.setAttribute("successMessage", localizationService.getLocaleString("successMessage"));
        session.setAttribute("profileInfoTitle", localizationService.getLocaleString("profileInfoTitle"));
        session.setAttribute("loginField", localizationService.getLocaleString("loginField"));
        session.setAttribute("loginPage", localizationService.getLocaleString("loginPage"));

        session.setAttribute("serviceListTitle", localizationService.getLocaleString("serviceListTitle"));
        session.setAttribute("myAccountInfo", localizationService.getLocaleString("myAccountInfo"));
        session.setAttribute("logoutTitle", localizationService.getLocaleString("logoutTitle"));
        session.setAttribute("donateTitle", localizationService.getLocaleString("donateTitle"));

        dispatcher = this.getServletContext().getRequestDispatcher("/login.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        String email = request.getParameter("email");
        String pass = request.getParameter("password");
        boolean valid = UserLoginFieldsValidator.validate(email, pass);

        boolean isPresent;
        boolean isAdmin;
        boolean isConfirmed;

        Connection connection = Pool.getConnection();
        new CheckUserInDB(connection);
        isPresent = CheckUserInDB.check(email, pass);
        new CheckAdminEmail(connection);
        isAdmin = CheckAdminEmail.check(email);
        new CheckUserConfirmed(connection);
        isConfirmed = CheckUserConfirmed.check(email, pass);

        TimerRunner timerRunner = null;
        try {
            timerRunner = new TimerRunner(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (timerRunner != null) {
            timerRunner.run();
        }

        RequestDispatcher dispatcher;
        if (!valid) {
            LOGGER.warn("Incorrect user input.");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            LOGGER.warn("Redirect to validation error page.");
            dispatcher = this.getServletContext().getRequestDispatcher("/validError.jsp");
            dispatcher.forward(request, response);
        } else if (isPresent) {
            if (isConfirmed) {
                userService = new UserDAOImpl(connection);
                User user = userService.get(email);
                Cookie session_id_coockie = new Cookie("session_id", session.getId());
                response.addCookie(session_id_coockie);
                session.setAttribute("email", user.getEmail());
                if (isAdmin) {
                    Cookie roleCoockie = new Cookie("role", user.getRole());
                    response.addCookie(roleCoockie);
                    session.setAttribute("role", user.getRole());
                    dispatcher = this.getServletContext().getRequestDispatcher("/adminHomeView.jsp");
                    dispatcher.forward(request, response);
                    LOGGER.info("User " + user.getEmail() + ",role " + user.getRole() + " logged in.");
                } else {
                    Cookie roleCoockie = new Cookie("role", user.getRole());
                    response.addCookie(roleCoockie);
                    session.setAttribute("role", user.getRole());
                    LOGGER.info("User " + user.getEmail() + ",role " + user.getRole() + " logged in.");
                    dispatcher = this.getServletContext().getRequestDispatcher("/userInfo");
                    dispatcher.forward(request, response);
                }
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                LOGGER.info("User email " + email + " is not confirmed.");
                dispatcher = this.getServletContext().getRequestDispatcher("/confirmUserWarningView.jsp");
                dispatcher.forward(request, response);
            }
        } else {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            LOGGER.info("User email " + email + " is not registered.");
            dispatcher = this.getServletContext().getRequestDispatcher("/registration.jsp");
            dispatcher.forward(request, response);
        }
    }
}
