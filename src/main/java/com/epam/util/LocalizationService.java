package com.epam.util;

import java.util.Locale;
import java.util.ResourceBundle;

public class LocalizationService {
    private Locale locale;
    private ResourceBundle.Control utf8Control = new Utf8Control();
    private ResourceBundle messages;

    public LocalizationService(Locale locale) {
        this.locale = locale;
        messages = ResourceBundle.getBundle("messages", locale, utf8Control);
    }

    public LocalizationService(String localeString) {
        if (localeString != null) {
            String[] languageCountry = localeString.split("_");
            locale = new Locale(languageCountry[0], languageCountry[1]);
        }
        messages = ResourceBundle.getBundle("messages", locale, utf8Control);
    }

    public String getLocaleString(String messageName) {
        return messages.getString(messageName);
    }
}
