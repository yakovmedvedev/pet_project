package com.epam.services.download;

import com.epam.servlets.LoginServlet;
import com.itextpdf.text.DocumentException;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xhtmlrenderer.resource.XMLResource;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


public class DownloadPDFFileService implements FileDownload {
    private static Logger LOGGER = LoggerFactory.getLogger(LoginServlet.class);

    public DownloadPDFFileService() {
    }

    @Override
    public void downloadFile() {

    }

    @Override
    public void downloadFile(String url) {
        try {
            String documentAsString = Jsoup.connect(url).get().html();
            ITextRenderer renderer = new ITextRenderer();
            org.w3c.dom.Document document = XMLResource.load(new ByteArrayInputStream(documentAsString.getBytes())).getDocument();
            renderer.setDocument(document, null);
            renderer.layout();
            String pathToDownloads = System.getProperty("user.home") + "\\Downloads\\";
            String fileNameWithPath = pathToDownloads + "ServicesPDF.pdf";
            FileOutputStream fos = new FileOutputStream(fileNameWithPath);
            renderer.createPDF(fos);
            fos.close();
            LOGGER.info("File 1: '" + fileNameWithPath + "' created.");
        } catch (DocumentException | IOException e) {
            LOGGER.error(e.getMessage());
        }
    }
}