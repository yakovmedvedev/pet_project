package com.epam.services.checking;

import com.epam.connection.PoolH2DB;
import com.epam.dao.implementation.UserDAOImpl;
import com.epam.dao.interfaces.UserDAO;
import com.epam.entities.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.assertFalse;

public class CheckUserConfirmedTest {
    private Connection connection = PoolH2DB.getConnection();
    private UserDAO userService = new UserDAOImpl(connection);

    public CheckUserConfirmedTest() throws SQLException {
    }

    @Before
    public void clearTableBefore() throws SQLException {
        try (Statement ps = connection.createStatement()) {
            ps.execute("TRUNCATE TABLE USERS");
        }
    }

    @Test
    public void checkIsUserConfirmed() {
        User u1 = new User("Gary", "Oldman", "boom@mail.ru", "222222");
        userService.add(u1);
        new CheckUserConfirmed(connection);
        assertFalse(CheckUserConfirmed.check(u1.getEmail(), u1.getPassword()));
    }

    @After
    public void clearTable() throws SQLException {
        try (Statement ps = connection.createStatement()) {
            ps.execute("TRUNCATE TABLE USERS");
        }
    }
}