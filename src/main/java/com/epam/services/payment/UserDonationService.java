package com.epam.services.payment;

import com.epam.dao.implementation.UserDAOImpl;
import com.epam.dao.interfaces.UserDAO;
import com.epam.entities.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;

public class UserDonationService {
    private Connection connection;
    private UserDAO userDAO;

    public UserDonationService(Connection connection) {
        this.connection = connection;
        this.userDAO = new UserDAOImpl(connection);
    }

    public void donate(String userEmail, int donation) {
        User tempUser = userDAO.get(userEmail);
        tempUser.setAmount(tempUser.getAmount() + donation);
        userDAO.update(userEmail, tempUser);
        try {
            AmountControlService amountControlService = new AmountControlService(userEmail, connection);
            amountControlService.run();
        } catch (ParseException | SQLException e) {
            e.printStackTrace();
        }
    }
}
