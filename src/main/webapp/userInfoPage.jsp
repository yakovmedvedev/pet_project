<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User Info Page</title>
</head>
<body>
<%
    Cookie[] cookies = request.getCookies();
    String roleNameCookie = "";
    for (Cookie c : cookies) {
        if (c.getName().equals("role")) {
            roleNameCookie = c.getValue();
        }
    }

    if (!(roleNameCookie.equals("CLIENT"))) {
        response.sendRedirect("adminHomeView.jsp");
    }
%>
<div class="data">
    <a href="${pageContext.request.contextPath}/userInfo?locale=en_ENG">ENG</a>
    <a href="${pageContext.request.contextPath}/userInfo?locale=ru_RU">RUS</a>
</div>
<jsp:include page="userMenuView.jsp"></jsp:include>

<h3>${helloTitle} ${user.firstName}</h3>

${fNameField} <b>${user.firstName}</b><br/>
${sNameField} <b>${user.secondName}</b><br/>
${emailField} <b>${user.email}</b><br/>
${amountField} <b>${user.amount}</b><br/>
${blockedTitle} <b>${user.blocked}</b><br/>
${registrationDateTitle} <b>${user.registrationDate}</b><br/>

<h3>${connectedServicesTitle}</h3>
<table border="1" cellpadding="5" cellspacing="1">
    <tr>
        <th>${serviceNameTitle}</th>
        <th>${tariffTitle}</th>
    </tr>
    <c:forEach items="${services}" var="service">
        <tr>
            <td>${service.serviceName}</td>
            <td>${service.tariff}</td>
            <td>
                <a href="userDeleteService?serviceName=${service.serviceName}&email=${user.email}">${deleteButton}</a>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
<div>
    <jsp:include page="copyrightTag.jsp"></jsp:include>
</div>
</html>
