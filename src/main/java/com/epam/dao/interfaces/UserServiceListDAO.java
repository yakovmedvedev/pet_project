package com.epam.dao.interfaces;

import com.epam.entities.Service;

import java.util.List;

public interface UserServiceListDAO {

    void addServiceToUser(String userEmail, String serviceName);

    Service getServiceToUser(String userEmail, String serviceName);

    void removeServiceFromUser(String userEmail, String serviceName);

    List<Service> getAllServiceToUser(String userEmail);

    List<Service> getAllServiceOrderedByName();

    List<Service> getAllServiceOrderedByPrice();
}
