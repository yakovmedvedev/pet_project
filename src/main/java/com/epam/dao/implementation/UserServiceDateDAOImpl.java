package com.epam.dao.implementation;

import com.epam.dao.interfaces.UserServiceDateDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserServiceDateDAOImpl implements UserServiceDateDAO {

    private Connection connection;

    public UserServiceDateDAOImpl() {
    }

    public UserServiceDateDAOImpl(Connection connection) throws SQLException {
        this.connection = connection;
    }

    @Override
    public String getStartDate(String email, String serviceName) {
        String startDate = null;
        try {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT start_term FROM users_services WHERE email=? and service=?");
            ps.setString(1, email);
            ps.setString(2, serviceName);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                startDate = resultSet.getString("start_term");
            }
            ps.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return startDate;
    }

    @Override
    public String getEndDate(String email, String serviceName) {
        String endDate = null;
        try {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT end_term FROM users_services WHERE email=? and service=?");
            ps.setString(1, email);
            ps.setString(2, serviceName);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                endDate = resultSet.getString("end_term");
            }
            ps.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return endDate;
    }

    @Override
    public void setStartAndEndDate(String email, String serviceName, String startDate, String endDate) {
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE users_services SET start_term = ?, end_term = ? WHERE email = ? AND service=?");
            statement.setString(1, startDate);
            statement.setString(2, endDate);
            statement.setString(3, email);
            statement.setString(4, serviceName);
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
