<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Validation ERROR</title>
</head>
<body>
<%
    session.invalidate();
    request.getSession(false);
%>

<h3>Validation ERROR !<br></h3>
Check the correctness of your email and password:<br>
1.'email' field must be not empty and contain correct email address.<br>
2.Password must be not empty and password length more then 4 characters.<br>
3.First Name and Second Name fields must be not empty and contain only letters.<br>
<a href="login.jsp">Login</a>
<a href="registration.jsp">Registration</a>

</body>
</html>