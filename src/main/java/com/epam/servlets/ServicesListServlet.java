package com.epam.servlets;

import com.epam.connection.Pool;
import com.epam.dao.implementation.ServiceDAOImpl;
import com.epam.entities.Service;
import com.epam.util.LocalizationService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/serviceList")
public class ServicesListServlet extends HttpServlet {

    private ServiceDAOImpl serviceDAO = new ServiceDAOImpl(Pool.getConnection());

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        resp.setStatus(HttpServletResponse.SC_OK);
        HttpSession session = req.getSession();
        List<Service> serviceList = serviceDAO.getAll();
        session.setAttribute("servicesList", serviceList);

        String dUrl = req.getRequestURL().toString();
        req.setAttribute("durl", dUrl);
        LocalizationService localizationService;
        String localizationParam = req.getParameter("locale");
        if (localizationParam == null) {
            localizationService = new LocalizationService(req.getLocale());
        } else {
            localizationService = new LocalizationService(localizationParam);
        }

        session.setAttribute("orderByNameTitle", localizationService.getLocaleString("orderByNameTitle"));
        session.setAttribute("orderByPriceTitle", localizationService.getLocaleString("orderByPriceTitle"));
        session.setAttribute("downloadTariffsTitle", localizationService.getLocaleString("downloadTariffsTitle"));
        session.setAttribute("downloadHtmlTitle", localizationService.getLocaleString("downloadHtmlTitle"));
        session.setAttribute("addButton", localizationService.getLocaleString("addButton"));
        session.setAttribute("serviceNameTitle", localizationService.getLocaleString("serviceNameTitle"));
        session.setAttribute("tariffTitle", localizationService.getLocaleString("tariffTitle"));

        session.setAttribute("serviceListTitle", localizationService.getLocaleString("serviceListTitle"));
        session.setAttribute("myAccountInfo", localizationService.getLocaleString("myAccountInfo"));
        session.setAttribute("logoutTitle", localizationService.getLocaleString("logoutTitle"));
        session.setAttribute("donateTitle", localizationService.getLocaleString("donateTitle"));

        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/servicesList.jsp");
        dispatcher.forward(req, resp);
    }
}
