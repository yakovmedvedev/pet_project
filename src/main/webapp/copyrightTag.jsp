<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/tag/copyright.tld" prefix="custom" %>

<html>
<head>
    <title>
        Copyright Tag
    </title>
</head>
<body>
<h4><custom:copyright name='Yakov Medvedev'/></h4>
</body>
</html>

