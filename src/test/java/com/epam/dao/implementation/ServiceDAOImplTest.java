package com.epam.dao.implementation;

import com.epam.connection.PoolH2DB;
import com.epam.dao.interfaces.ServiceDAO;
import com.epam.entities.Service;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ServiceDAOImplTest {

    private static Connection connection = PoolH2DB.getConnection();
    private ServiceDAO service = new ServiceDAOImpl(connection);

    @BeforeClass
    public static void setReferentialIntegrity() throws SQLException {
        PreparedStatement ps = connection.prepareStatement("SET REFERENTIAL_INTEGRITY FALSE");
        ps.execute();
        ps.close();
    }

    @Before
    public void clearTableBefore() throws SQLException {
        PreparedStatement ps = connection.prepareStatement("TRUNCATE TABLE SERVICES");
        ps.execute();
        ps.close();
    }

    @Test
    public void addServiceTest() {
        Service s1 = new Service("New Service", 220);
        service.add(s1);
        Service s2 = service.get(s1.getServiceName());
        assertEquals(s1.getServiceName(), s2.getServiceName());
        assertEquals(s1.getTariff(), s2.getTariff());
    }

    @Test
    public void updateServiceTest() throws Exception {
        Service oldService = new Service("Old Service", 220);
        Service newService = new Service("New Service", 330);
        service.add(oldService);
        service.update(oldService.getServiceName(), newService);
        Service getNewService = service.get(newService.getServiceName());
        assertEquals(newService.getServiceName(), getNewService.getServiceName());
        assertEquals(newService.getTariff(), getNewService.getTariff());
    }

    @Test
    public void getAllServiceTest() throws Exception {
        Service s1 = new Service("Old Service", 220);
        Service s2 = new Service("New Service", 330);
        Service s3 = new Service("Other Service", 440);
        service.add(s1);
        service.add(s2);
        service.add(s3);
        List<Service> list = service.getAll();
        assertEquals(3, list.size());
    }

    @Test
    public void getServiceTest() throws Exception {
        Service newService = new Service("New Service", 330);
        service.add(newService);
        Service getAddedNewService = service.get("New Service");
        assertEquals(newService.getServiceName(), getAddedNewService.getServiceName());
        assertEquals(newService.getTariff(), getAddedNewService.getTariff());
    }

    @Test
    public void deleteServiceTest() throws Exception {
        Service s = new Service("New Service", 330);
        service.add(s);
        service.delete(s.getServiceName());
        assertNull(service.get(s.getServiceName()));
    }

    @After
    public void clearTable() throws SQLException {
        try (Statement ps = PoolH2DB.getConnection().createStatement()) {
            ps.execute("TRUNCATE TABLE SERVICES");
        }
    }
}