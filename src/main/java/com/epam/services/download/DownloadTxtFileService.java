package com.epam.services.download;

import com.epam.dao.implementation.ServiceDAOImpl;
import com.epam.dao.interfaces.ServiceDAO;
import com.epam.entities.Service;
import com.epam.servlets.LoginServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

public class DownloadTxtFileService implements FileDownload {
    private Connection connection;
    private Logger LOGGER = LoggerFactory.getLogger(LoginServlet.class);
    private ServiceDAO serviceDAO;

    public DownloadTxtFileService(Connection connection) {
        this.connection = connection;
        serviceDAO = new ServiceDAOImpl(connection);
    }

    @Override
    public void downloadFile() throws IOException {
        String pathToDownloads = System.getProperty("user.home") + "\\Downloads\\serviceTariffs.txt";
        List<Service> serviceList = serviceDAO.getAll();
        FileWriter writer = new FileWriter(pathToDownloads);
        for (Service service : serviceList) {
            writer.write(service.toString() + "\n");
        }
        writer.write(String.valueOf(new Date()));
        writer.close();
        LOGGER.info("File " + pathToDownloads + " was downloaded.");
    }

    @Override
    public void downloadFile(String url) {
    }
}

