<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Avaliable Services List</title>
</head>
<body>
<%
    Cookie[] cookies = request.getCookies();
    String roleNameCookie = "";
    for (Cookie c : cookies) {
        if (c.getName().equals("role")) {
            roleNameCookie = c.getValue();
        }
    }

    if (!(roleNameCookie.equals("ADMINISTRATOR"))) {
        response.sendRedirect("userInfoPage.jsp");
    }
%>
<jsp:include page="adminMenu.jsp"></jsp:include>
<table border="1" cellpadding="5" cellspacing="1">
    <tr>
        <th>Service Name</th>
        <th>Tariff</th>

    </tr>
    <c:forEach items="${adminServicesList}" var="services">
        <tr>
            <td>${services.serviceName}</td>
            <td>${services.tariff}</td>
            <td>
                <a href="${pageContext.request.contextPath}/editService?oldServiceName=${services.serviceName}&oldServiceTariff=${services.tariff}">Edit</a>
            </td>
            <td>
                <a href="${pageContext.request.contextPath}/deleteService?serviceName=${services.serviceName}">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>
<br>
<a href="${pageContext.request.contextPath}/adminAddNewServiceView.jsp">Add new Service</a>
<div>
    <jsp:include page="copyrightTag.jsp"></jsp:include>
</div>
</body>
</html>
