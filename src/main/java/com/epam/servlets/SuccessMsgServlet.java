package com.epam.servlets;

import com.epam.util.LocalizationService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(urlPatterns = "/successMsg")
public class SuccessMsgServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        LocalizationService localizationService;
        String localizationParam = req.getParameter("locale");
        if (localizationParam == null) {
            localizationService = new LocalizationService(req.getLocale());
        } else {
            localizationService = new LocalizationService(localizationParam);
        }

        session.setAttribute("successMessage", localizationService.getLocaleString("successMessage"));
        session.setAttribute("loginField", localizationService.getLocaleString("loginField"));

        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/success.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
