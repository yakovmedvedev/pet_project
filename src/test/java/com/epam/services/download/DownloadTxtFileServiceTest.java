package com.epam.services.download;

import com.epam.connection.PoolH2DB;
import com.epam.dao.implementation.ServiceDAOImpl;
import com.epam.dao.interfaces.ServiceDAO;
import com.epam.entities.Service;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.assertTrue;

public class DownloadTxtFileServiceTest {
    private Connection connection = PoolH2DB.getConnection();
    private ServiceDAO service = new ServiceDAOImpl(connection);

    public DownloadTxtFileServiceTest() throws SQLException {
    }

    @Before
    public void clearTableBefore() throws SQLException {
        try (Statement ps = connection.createStatement()) {
            ps.execute("TRUNCATE TABLE SERVICES");
        }
    }

    @Test
    public void testIfdownloadedFileExist() throws IOException {
        Service s1 = new Service("Old Service", 220);
        Service s2 = new Service("New Service", 330);
        Service s3 = new Service("Other Service", 440);
        service.add(s1);
        service.add(s2);
        service.add(s3);
        DownloadTxtFileService downloadTxtFileService = new DownloadTxtFileService(connection);
        downloadTxtFileService.downloadFile();
        Path pathToTxtFile = Paths.get(System.getProperty("user.home") + "\\Downloads\\serviceTariffs.txt");
        assertTrue(Files.exists(pathToTxtFile));
    }

    @After
    public void clearTable() throws SQLException {
        try (Statement ps = connection.createStatement()) {
            ps.execute("TRUNCATE TABLE SERVICES");
        }
    }
}