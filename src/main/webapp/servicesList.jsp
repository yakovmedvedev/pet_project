<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Avaliable Services List</title>
</head>
<body>

<div class="data">
    <a href="${pageContext.request.contextPath}/serviceList?locale=en_ENG">ENG</a>
    <a href="${pageContext.request.contextPath}/serviceList?locale=ru_RU">RUS</a>
</div>

<jsp:include page="userMenuView.jsp"></jsp:include>
<table border="1" cellpadding="5" cellspacing="1">
    <tr>
        <th>${serviceNameTitle}</th>
        <th>${tariffTitle}</th>
    </tr>
    <c:forEach items="${servicesList}" var="services">
        <tr>
            <td>${services.serviceName}</td>
            <td>${services.tariff}</td>
            <td>
                <a href="${pageContext.request.contextPath}/addServiceToUser?serviceName=${services.serviceName}&email=${user.email}">${addButton}</a>
            </td>

        </tr>
    </c:forEach>
</table>

<a href="${pageContext.request.contextPath}/getOrderedList?order=byName">${orderByNameTitle}</a>
<a href="${pageContext.request.contextPath}/getOrderedList?order=byPrice">${orderByPriceTitle}</a></br>
<a href="${pageContext.request.contextPath}/downloadTxt">${downloadTariffsTitle}</a>
<a href="${pageContext.request.contextPath}/downloadHtml?url=<%=request.getAttribute("durl")%>">${downloadHtmlTitle}</a>
<div>
    <jsp:include page="copyrightTag.jsp"></jsp:include>
</div>
</body>
</html>
