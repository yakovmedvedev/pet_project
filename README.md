EPAM COURSE #PETPROJECT
===========
Stack of technologies:
------------

* Java 8
* MVC architecture
* JDBC
* Servlet API
* JSP/JSTL
* MariaDB
* Liquibase
* Maven
