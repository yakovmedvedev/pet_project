package com.epam.servlets;

import com.epam.connection.Pool;
import com.epam.dao.implementation.ServiceDAOImpl;
import com.epam.entities.Service;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


@WebServlet(urlPatterns = "/adminServiceList")
public class AdminServicesListServlet extends HttpServlet {
    private ServiceDAOImpl serviceDAO = new ServiceDAOImpl(Pool.getConnection());

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        resp.setStatus(HttpServletResponse.SC_OK);
        HttpSession session = req.getSession();
        List<Service> adminServicesList = serviceDAO.getAll();
        session.setAttribute("adminServicesList", adminServicesList);
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/adminServiceList.jsp");
        dispatcher.forward(req, resp);
    }
}
