package com.epam.servlets;

import com.epam.connection.Pool;
import com.epam.dao.implementation.ServiceDAOImpl;
import com.epam.entities.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/editService")
public class EditServiceServlet extends HttpServlet {

    private static Logger LOGGER = LoggerFactory.getLogger(EditServiceServlet.class);
    private ServiceDAOImpl serviceDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String oldServiceName = req.getParameter("oldServiceName");
        String oldServiceTariff = req.getParameter("oldServiceTariff");
        req.setAttribute("oldServiceName", oldServiceName);
        req.setAttribute("oldServiceTariff", oldServiceTariff);
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/adminEditServiceView.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String oldServiceName = req.getParameter("oldServName");
        String newServiceName = req.getParameter("newServiceName");
        String newServiceTariff = req.getParameter("newServiceTariff");
        Connection connection = Pool.getConnection();
        serviceDAO = new ServiceDAOImpl(connection);
        Service newService = new Service(newServiceName, Integer.valueOf(newServiceTariff));
        serviceDAO.update(oldServiceName, newService);
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        LOGGER.info("Service " + oldServiceName + " changed to " + newServiceName + " ,new tariff  " + newServiceTariff);
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/adminHomeView.jsp");
        dispatcher.forward(req, resp);
    }
}


