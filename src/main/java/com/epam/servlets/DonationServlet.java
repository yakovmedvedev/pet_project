package com.epam.servlets;

import com.epam.connection.Pool;
import com.epam.services.payment.AmountControlService;
import com.epam.services.payment.UserDonationService;
import com.epam.services.validation.UserLoginFieldsValidator;
import com.epam.util.LocalizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;

@WebServlet(urlPatterns = "/donate")
public class DonationServlet extends HttpServlet {

    private static Logger LOGGER = LoggerFactory.getLogger(DonationServlet.class);
    private UserDonationService donationService;
    private AmountControlService amountControlService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = null;
        String userEmail = req.getParameter("email");
        String donationInString = req.getParameter("donation");

        Connection connection = Pool.getConnection();
        donationService = new UserDonationService(connection);
        try {
            amountControlService = new AmountControlService(userEmail, connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (UserLoginFieldsValidator.validateNumberField(donationInString)) {
            int donationAmount = Integer.valueOf(donationInString);
            donationService.donate(userEmail, donationAmount);
            try {
                amountControlService.run();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            LOGGER.info("User " + req.getParameter("email") + " donated " + donationAmount + " UAH.");

            dispatcher = req.getServletContext().getRequestDispatcher("/userInfoPage.jsp");
            dispatcher.forward(req, resp);
        } else {
            LOGGER.error("Donation ERROR.");
            dispatcher = req.getServletContext().getRequestDispatcher("/userDonationErrorView.jsp");
            dispatcher.forward(req, resp);
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        LocalizationService localizationService;
        String localizationParam = req.getParameter("locale");
        if (localizationParam == null) {
            localizationService = new LocalizationService(req.getLocale());
        } else {
            localizationService = new LocalizationService(localizationParam);
        }

        session.setAttribute("donatePageTitle", localizationService.getLocaleString("donatePageTitle"));
        session.setAttribute("donateTitle", localizationService.getLocaleString("donateTitle"));
        session.setAttribute("enterTheAmount", localizationService.getLocaleString("enterTheAmount"));
        session.setAttribute("cancelbutton", localizationService.getLocaleString("cancelbutton"));

        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/userDonationView.jsp");
        dispatcher.forward(req, resp);
    }
}
