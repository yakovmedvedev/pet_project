package com.epam.dao.interfaces;

import com.epam.entities.User;

public interface UserDAO extends DAO<User> {
}
