INSERT INTO users
(f_name,s_name,email,password,amount,confirmed,blocked,role,registration_date)
VALUES
  ('Jacob','Medvedev','medved@gmail.com','112233',300,true,false,'ADMINISTRATOR','01.01.2018'),
  ('Alex','Lee','lee@gmail.com','222222',100,false,true,'CLIENT','13.10.2018'),
  ('Ihor','Dorn','dorn@gmail.com','666666',0,false,true,'CLIENT','01.10.2018');