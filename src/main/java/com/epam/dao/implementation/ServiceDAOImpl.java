package com.epam.dao.implementation;

import com.epam.dao.interfaces.ServiceDAO;
import com.epam.entities.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ServiceDAOImpl implements ServiceDAO {

    private Connection connection;

    public ServiceDAOImpl() {
    }

    public ServiceDAOImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void add(Service element) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO services (name,tariff) VALUES (?,?)");
            statement.setString(1, element.getServiceName());
            statement.setInt(2, element.getTariff());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void update(String oldElement, Service newElement) {
        try {
            PreparedStatement ps = connection.prepareStatement("UPDATE services SET name=?, tariff=? WHERE name=?");
            ps.setString(1, newElement.getServiceName());
            ps.setInt(2, newElement.getTariff());
            ps.setString(3, oldElement);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<Service> getAll() {
        List<Service> result = null;
        try {
            Statement st = connection.createStatement();
            ResultSet resultSet = st.executeQuery("SELECT * FROM services");
            result = new ArrayList<>();
            while (resultSet.next()) {
                Service service = new Service(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("tariff"));
                result.add(service);
            }
            st.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    public Service get(String name) {
        Service service = null;
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM services WHERE name=?");
            ps.setString(1, name);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                service = new Service(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("tariff"));
            }
            ps.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return service;
    }

    @Override
    public Service getById(int elementId) {
        Service service = null;
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM services WHERE id=?");
            ps.setInt(1, elementId);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                service = new Service(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("tariff"));
            }
            ps.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return service;
    }

    @Override
    public void delete(String elementName) {
        try {
            PreparedStatement ps = connection.prepareStatement("DELETE FROM services WHERE name=?");
            ps.setString(1, elementName);
            ps.execute();
            ps.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
