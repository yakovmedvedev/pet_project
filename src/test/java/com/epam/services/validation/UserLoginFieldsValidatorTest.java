package com.epam.services.validation;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class UserLoginFieldsValidatorTest {

    @Test
    public void validateUserLoginForm() {
        boolean result = UserLoginFieldsValidator.validate("medved@mail.ru", "111111");
        assertTrue(result);
    }

    @Test
    public void validateUserRegistrationFormEN() {
        boolean result = UserLoginFieldsValidator.validate("medved@mail.ru", "111111", "Bruce", "Lee");
        assertTrue(result);
    }

    @Test
    public void validateUserRegistrationFormRU() {
        boolean result = UserLoginFieldsValidator.validate("sidorow@mail.ru", "333333", "Иван", "Сидоров");
        assertTrue(result);
    }


    @Test
    public void validateDonateString() {
        boolean result = UserLoginFieldsValidator.validateNumberField("111111");
        assertTrue(result);
    }
}