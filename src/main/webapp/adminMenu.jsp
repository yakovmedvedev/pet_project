<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin Menu</title>
</head>
<body>
<%
    Cookie[] cookies = request.getCookies();
    String roleNameCookie = "";
    for (Cookie c : cookies) {
        if (c.getName().equals("role")) {
            roleNameCookie = c.getValue();
        }
    }

    if (!(roleNameCookie.equals("ADMINISTRATOR"))) {
        response.sendRedirect("userInfoPage.jsp");
    }
%>
<a href="${pageContext.request.contextPath}/getAllUsers">All Users</a>
|
<a href="${pageContext.request.contextPath}/unconfirmedUsers">UnConfirmed Users</a>
|
<a href="${pageContext.request.contextPath}/adminServiceList">Services</a>
|
<a href="${pageContext.request.contextPath}/blockingUsers">Users Blocking</a>
|
<a href="${pageContext.request.contextPath}/logout">Logout</a>
</body>
</html>
