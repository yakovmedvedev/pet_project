package com.epam.services.checking;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CheckUserConfirmed {
    private static Connection connection;

    public CheckUserConfirmed(Connection connection) {
        CheckUserConfirmed.connection = connection;
    }

    public static boolean check(String email, String pass) {
        boolean result = false;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("SELECT confirmed FROM users WHERE email=? AND password=?");
            ps.setString(1, email);
            ps.setString(2, pass);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                result = resultSet.getBoolean("confirmed");
            }
            ps.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}

