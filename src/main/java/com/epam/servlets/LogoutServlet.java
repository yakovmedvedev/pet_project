package com.epam.servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet(urlPatterns = "/logout")
public class LogoutServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        Cookie[] cookies = req.getCookies();
        for (Cookie c : cookies) {
            if (c.getName().equals("role")) {
                c.setMaxAge(0);
            }
        }
        session.invalidate();
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/userLogin");
        dispatcher.forward(req, resp);
    }
}
